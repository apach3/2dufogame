﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    //Hold player object reference
    public GameObject Player;

    //offset between player and camera
    private Vector3 _offset;

	// Use this for initialization
	private void Start () {
        //take current transform of the camera and the current transform 
        //of the player and find difference between the two
	    _offset = transform.position - Player.transform.position;
	}
	
	/// <summary>
    /// Runs every frame, but is guaranteed to run after Update is completed.
    /// </summary>
	private void LateUpdate () {
		//set transfrom position of our camera to our player transform position + the calculated offset
	    transform.position = Player.transform.position + _offset;
	}
}
