﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    //holds speed variable for movement
    public float Speed;

    //UI.Text object for displaiyng score
    public Text ScoreText;

    //UI.Text object for displayeing win message
    public Text WinText;

    //holds attached Rigidbody2D reference
    private Rigidbody2D _rb2D;

    //holds count of collectibles
    private int _score;

    /// <summary>
    /// Called before any frame is rendered. Ofter, the begining of a game.
    /// Assign private variables.
    /// </summary>
    private void Start()
    {
        //set private variables
        _rb2D = GetComponent<Rigidbody2D>();
        _score = 0;

        //Display score in UI.Text object
        DisplayScore();
        //Set win text to empty string
        WinText.text = "";
    }

    /// <summary>
    /// Called every frame. FixedUpdate is used for physics calculations.
    /// </summary>
    private void FixedUpdate()
    {
        //movement variables
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");
        var movement = new Vector2(moveHorizontal, moveVertical);

        _rb2D.AddForce(movement * Speed);
    }

    /// <summary>
    /// Called by Unity when Player object touches another 2D collider.
    /// </summary>
    /// <param name="collider">Other 2D collider object.</param>
    private void OnTriggerEnter2D(Collider2D otherCollider)
    {
        //if the collider isn't tagged with PickUp tag, exit
        if (!otherCollider.gameObject.CompareTag("PickUp")) return;

        //set the gameobject of our collider inactive
        otherCollider.gameObject.SetActive(false);
        //increment player score
        _score = _score + 1;
        //display score in UI.Text element
        DisplayScore();
        //if we collected all pick ups display Win message
        if (_score >= 8) DisplayWinMessage();
    }

    /// <summary>
    /// Display _score variable in games UI.
    /// </summary>
    private void DisplayScore()
    {
        ScoreText.text = "Score: " + _score.ToString();
    }

    /// <summary>
    /// Display message in UI when all pick ups are collected.
    /// </summary>
    private void DisplayWinMessage()
    {
        WinText.text = "You Win!";
    }
}
