﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    /// <summary>
    /// Rotate the object every frame.
    /// We are not using forces, so we can use Update instead of FixedUpdate.
    /// </summary>
    void Update ()
    {
        //rotate the object around the Z axis.
        //multiply the movement with deltaTime to make it smooth and frame independent.
        transform.Rotate(new Vector3(0,0,45) * Time.deltaTime);
    }
}
